# The Innovator's Solution

**"This book is about how to create new growth in business."**

On why the growth is never fast enough in the perspective of investors:

**"even if a company's core business is growing vigorously, the only way its managers can deliver a rate of return to shareholders in the future that exceeds the risk adjusted market average is to grow faster than the shareholders expect. (...) A company must deliver the rate of growth that the market is projecting just to keep its stock price from falling. It must exceed the consensus forecast rate of growth in order to boost its share price."**

- Predictability comes from **Good Theory**

**How Theories are built:**

1. Describe the phenomenon we wish to understand
2. Classify the phenomenon in categories
3. Articulate a theory that asserts what causes the phenomenon to occur, and why
