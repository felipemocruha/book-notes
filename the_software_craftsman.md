# The Software craftsman


## About "seniority"

> Seniority is not just about writing code or being veryfamiliar with a language or framework. Modern developers need to know how to speak to customers, automate tests anddeployment, make technological choices that may affect the entire business, work in distributed teams, help clients todefine and prioritize requirements, report progress, manage changes and expectations, present products to potentialclients or partners, help with pre-sales activities, estimate time and costs, interview new team members, design andevolve the software architecture, deal with nonfunctional requirements and service level agreements (SLAs), understandbusiness goals, make decisions based on trade-offs, keep an eye on new technologies and better ways to do things, worryabout the value delivered to their customers, and many other things.

## Don't "just code"

> The old attitude we had—if it isnot coding, it’s not my problem—is now unacceptable. Companies started to get leaner, flattening their hierarchies andreplacing many one-trick-pony professionals with generalizing specialists.

## On Agile

> The Agile transformations focused mainly on process; empowering people; reducing bureaucracy and waste;prioritization; visibility of work in progress; and improving the information flow. These were real and important problemsthat needed to be solved, and for sure, companies were far better off when they were addressed and improved. Withoutfixing these problems, it would be almost impossible to have a successful software project. However, the principles behindAgile were forgotten. Process became more important than technical excellence. Technical excellence, which is assumedto be in place by every process-related Agile methodology, is normally ignored by managers and ill-prepared Agile coaches.

> (...) the Toyota Dream. First,building cars is totally different from building software. We do not use a car if it is not totally finished. We do not go backto the manufacturer after buying a car and ask them to add an extra door to it or to move the engine from the front tothe back.

> When talking about Toyota, we very rarely think, “What if the cars were not good? What if no one wanted to buythem? What if they were so fragile and we had to take them to the garage every month?” An important factor to Toyota’ssuccess was, besides a good process, the focus on the quality of their cars.

>  Focusing purely onthe process and treating software development like a production line leads to average 9-to-5 developers doing what theyare told—they become factory workers. This approach makes the feedback system inefficient.

## On Software Craftsmanship

> Software Craftsmanship sees software as a craft and compares software developers to medievalblacksmiths. Apprentices would work with more experienced blacksmiths, traveling from place to place and working fordifferent masters, learning different tools and techniques, and improving their craft until the point where they were goodenough to become a master themselves.
