# quantitative trading

> The largest block of time I need to spend is in the morningbefore the market opens: I typically need to run various programs todownload and process the latest historical data, read company newsthat comes up on my alert screen, run programs to generate the or-ders for the day, and then launch a few baskets of orders beforethe market opens and start a program that will launch orders auto-matically throughout the day.

> I would also update my spreadsheetto record the previous day’s profit and loss (P&L) of the differentstrategies I run based on the brokerages’ statements. All of this takesabout two hours.After that, I spend another half hour near the market close to di-rect the programs to exit various positions, manually checking thatthose exit orders are correctly transmitted, and closing down vari-ous automated programs properly.In between market open and close, everything is supposed to beon autopilot. Alas, the spirit is willing but the flesh is weak: I oftencannot resist the urge to take a look (sometimes many looks) atthe intraday P&L of the various strategies on my trading screens. 

Sources of trading ideas:

Academic:
- Business schools’ finance professors’ -> websiteswww.hbs.edu/research/research.html
- Social Science Research Network -> www.ssrn.com
- National Bureau of Economic Research -> www.nber.org
- Business schools’ quantitative finance -> seminarswww.ieor.columbia.edu/seminars/financialengineering
- Mark Hulbert’s column in theNew YorkTimes’ Sunday business section -> www.nytimes.com
- Buttonwood column in theEconomistmagazine’s finance section -> www.economist.com


Financial web sites and blogs:
- Yahoo! Finance -> finance.yahoo.com
- TradingMarkets -> www.TradingMarkets.com
- Seeking Alpha -> www.SeekingAlpha.com
- TheStreet.com -> www.TheStreet.com
- The Kirk Report -> www.TheKirkReport.com
- Alea Blog -> www.aleablog.com
- Abnormal Returns -> www.AbnormalReturns.com
- Brett Steenbarger Trading Psychology -> www.brettsteenbarger.com
- My own -> epchan.blogspot.com

### How to find a strategy that suits you

- If trading part time, avoid intraday strategies or automate
- Have programming skills
- Have money available

![](img/capital_availability.png)

- Sharpe ratio

![](img/drawdown.png)


- Bollinger bands: every time the price exceeds plus or minus2 moving standard deviations of its moving average, short or buy, re-spectively. Exit the position when the price reverts back to within 1moving standard deviation of the moving average. If you allow your-self to enter and exit every five minutes, you will find that the Sharperatio is about 3 without transaction costs — very excellent indeed!Unfortunately, the Sharpe ratio is reduced to –3 if we subtract 1 ba-sis point as transaction costs, making it a very unprofitable strategy.


### survivorship bias

A historical database of stock prices that does not include stocksthat have disappeared due to bankruptcies, delistings, mergers, oracquisitions suffer from the so-called survivorship bias, becauseonly “survivors” of those often unpleasant events remain in thedatabase. (The same term can be applied to mutual fund or hedgefund databases that do not include funds that went out of business.)Backtesting a strategy using data with survivorship bias can be dan-gerous because it may inflate the historical performance of the strategy.

**Financial time series are a nonstationary process**

On machine learning based trading:

Neural networks,decision trees, and genetic algorithms. With many parameters, we can for surecapture small patterns that no human can see. But do these patterns persist?Or are they random noises that will never replay again? 

The strategies with AI that usually work:

- They are based on a sound econometric or rational basis, and not on ran-dom discovery of patterns.
- They have few parameters that need to be fitted to past data.
- They involve linear regression only, and not fitting to some esoteric non-linear functions.
- They are conceptually simple.
- All optimizations must occur in a lookback moving window, involving no fu-ture unseen data. And the effect of this optimization must be continuouslydemonstrated using this future, unseen data.

Cutting out unsuitable strategies:
- Does it outperform a benchmark?
- Does it have a high enough Sharpe ratio?
- Does it have a small enough drawdown and short enough draw-down duration?
- Does the backtest suffer from survivorship bias?
- Does the strategy lose steam in recent years compared to its earlier years?

**Find free or low cost historical databases**

> I recommend getting historical data that are already split anddividend adjusted, because otherwise you would have to find aseparate historical database (such as earnings.com) of splits anddividends and apply the adjustments yourself


Backtesting pitfalls:

- Look-ahead bias: How do we avoid look-ahead bias? Uselaggedhistorical datafor calculating signals at every opportunity. Lagging a series of datameans that you calculate all the quantities like moving averages,highs and lows, or even volume, based on data up to the close of theprevioustrading period only.

- Data-Snooping bias (overfitting):
  - Rule of thumb: the number of data points neededfor optimizing your parameters is equal to 252 times the number of free parameters your model has.

### Risk management

**Kelly formula to calculate optimal allocation of capital**
