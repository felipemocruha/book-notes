# Soft Skills

## chapter 2

- Think about your career with the mindset that it is a business you manage. Your employer is your customer.

## chapter 3

- Set and track career goals

Taking action:
- Sit down and write out at least one major goal for your career.
- Break down that major goal into smaller goals that correspond to
  - Months
  - Weeks
  - Days
  
## chapter 5

- Cultivate your network

Taking action:
- Even if you aren’t actively looking for a job right now, make a list of companies that you’d potentially like to work for and who you know at those companies.
- If there are companies on your list that you’d like to work for, but you don’t know anyone at those companies, come up with a plan to meet at least one person working at one of those companies and build a relationship with them.
- Find at least one local user group in your area and attend a meeting. Introduce yourself to as many people as possible.

## chapter 7

- Know your specialties

Taking action:
- List all the different kinds of software developer specialties you can think of. Go from broad to specific and see how specific you can get.
- What is your current specialty? If you don’t have one, think about what area of software development you could specialize in.
- Go to a popular job search website and look for jobs in your market for your specialty. Try to get an idea of whether or not further specialization would be beneficial to you or limit your choices too much.

## chapter 8

- Work at the right environment

Taking action:
- Take some time to think about what kind of environment you prefer working in. What company size matches your ideal working environment?
- Make a list of companies in your area or companies you have worked for and decide which category each fits into.


## chapter 9

- Get more responsibilities
- Make your work/results visible
- Keep improving yourself

## chapter 10

> Turning pro is a mindset. If we are struggling with fear, self-sabotage, procrastination, self-doubt, etc., the problem is, we’re thinking like amateurs. Amateurs don’t show up. Amateurs crap out. Amateurs let adversity defeat them. The pro thinks differently. He shows up, he does his work, he keeps on truckin’, no matter what.

The War of Art (Black Irish Books, 2002), Steven Pressfield

Be a professional:
- have principles
- get the job done
- prepare yourself to meetings
- manage your time
- say no

## chapter 11

Going solo:
- have a plan before quitting
- track your time
- be prepare to work harder even when doing the same working hours as being employed

- Taking action:Calculate exactly how much money you’ll need to earn each month to live. You might be surprised to find out how high it is right now. If you want to get “free” quicker, you’ll need to figure out a way to reduce that amount so that your side business will need to bring in less income.
- Start tracking your time every day at work. Get an idea of how you’re spending your time currently each day. Now, figure out how much of that time is actual productive time where you’re actually doing real hard productive work—you might be surprised by the results.

## chapter 12

- Consider additional costs and time when freelancing (contracts, bookkeeping, taxes...)
- You could need to double your income to keep the same lifestyle as a employed developer
- inbound marketing + matching specialtes + raising rates = more $$$$$

Taking action:
- Make a list of all the people you know who could potentially use your services or may know someone who can.
- Come up with an email template that you can send to everyone from the list you just created. (Remember to talk about what value you can bring, not just what you can do from a technical perspective.)
- Send out a message on your social networks and send your email to a small portion of the list you created and see what kind of response you get. Once you get some feedback, alter your email and send it out again to more people.

## chapter 13

- Create a product

Taking action:
- Come up with some target audiences you could investigate to create a potential product for.
- Pick one of these audiences and find out where members of that audience congregate, online or otherwise. Join some of their communities and listen to their problems. See if you can pick out one or two potential areas for a product that can solve a pain they have.
- Check who else may already be solving this problem. You don’t want to enter a market with too much competition.

## chapter 16

- Get big challenges

Taking action:
- Honest assessment time. What is your attitude in difficult situations? How do you deal with encountering the new and unfamiliar? Think about the last time you were in a difficult or unfamiliar situation and how you reacted.
- How can you create a more confident attitude without appearing arrogant? What is the difference? What steps can you take right now to improve your ability to fake it till you make it?

## chapter 18

- Professionals love nothing and hate everything

## chapter 19

> The right way to market yourself is to provide value to others. (...) the key to successfully marketing yourself in a way that makes others like you and want to work with you is to do it in a way that provides them value.

> there is a way to break away from the pack. By learning how to market yourself you can stand out from the crowd, and just like a famous rock star or celebrity chef, earn a much higher income and have many more opportunities than you would otherwise.

self marketing channels:
- Blog posts:	Either through your own blog or guest posts on other people’s blogs.
- Podcasts:	Create your own podcast or be interviewed on an existing podcast.
- Videos:	Create topical videos or screencasts and tutorials on sites like YouTube.
- Magazine: articles	Write an article for a software development magazine.
- Books: 	Write a book, like this one, or self-publish your own book.
- Code camps:	Most code camps will allow anyone to speak.
- Conferences:	A great way to network, and if you can speak at one of these events, even better.

## chapter 20

- create your personal brand

Steps for Creating a Brand:
- Define your message.
- Pick your niche.
- Create a tagline.
- Create an elevator pitch.
- Create visuals.


Taking action:
- Make a list of some of the popular brands you’re familiar with. Pick one or two to study in depth. Try to determine what their message is and look for how they use their logo and other visual elements to convey it.
- Brainstorm a list of niche ideas for your own personal brand. Come up with at least 10 to 15 ideas and then narrow down the list to your top 2 or 3. Try to come up with one that you’ll use for your personal brand.

## chapter 21

Blog

Taking action:
- What are your favorite developer blogs? Take a look at some of the blogs you read and see if you can figure out how often those blogs are updated with new posts and the average length of each post.
- If you don’t have a blog already, start one. Sign up today and create your first blog post. Come up with a schedule that you’ll stick to for writing future blog posts.
- Commit to keeping your blog up for at least a year. It takes time and commitment to get results. At about the one-year mark is when most people start to see some traction.

## chapter 28

10 step system to learn things:

1. Get the big picture
2. Determine scope
3. Define success
4. Find resources
5. Create a learning plan
6. Filter resources
7. Learn enough to get started
8. Play around
9. Learn enough to do something useful
10. Teach

## chapter 35

Fill the gaps

Taking action:
- For the next few days keep a pad of paper with you, and anytime you encounter something you don’t understand, write it down.
- Make a conscious effort to ask questions—even if they’re embarrassing—any time you don’t understand something in a conversation.
- Identify some “pain points” in your day and figure out ways you can get rid of them by filling in some gap in your knowledge.

## chapter 42

Keep motivated 

Taking action
- Think about all the unfinished projects and endeavors you undertook but never completed or mastered. What was it that made you quit? How do you feel now about this thing?
- Decide that next time you take on a project, you’re going to take to completion or mastery. Set up rules and constraints that will force you to overcome the walls you’ll inevitably hit.
- If you’re facing a wall of some sort in your career or personal life, try and push past it. Think about what might lay in store for you on the other side of the wall. Imagine that your motivation and interest will eventually return.


## chapter 49

Think about money

- Follow the cash that goes through your hands every month. See how much money you start with and where that money goes. Does most of your money go toward liabilities instead of investing in assets?
- Calculate how much money you’d have to save each year to reach 1 million dollars in the bank or whatever number you’d consider to be financially independent. Can you possibly save that much money in your lifetime without investing?
- Start asking yourself “How much can I save?” rather than “How much can I afford?”

## chapter ???

Salary negotiation

What about when you’re asked to name a number first?
Don’t do it. Just say “no.”

Yes, I know this is tough advice to follow, but let me give you some specific situations and some ways to deal with them.

First, you may be asked about your salary requirements before an interview or as a field on a job application. If you have a field on a job application, leave it blank if possible or simply put “negotiable depending on overall compensation package.” If you have to name a specific number, put $0 and then explain why later.

If you’re asked directly in a prescreening interview about what salary you require or are expecting, try to answer the same thing. Say it depends on the overall compensation including benefits. You may get a response stating what the benefit would be or that they just need a general number. In this case, you should try, as tactfully as possible, to turn the question around and ask a series of questions like the following:

“I’d rather learn more about your company and understand more about the job I’d be doing before naming an exact number or estimate, but it sounds like you’re trying to figure out if we’re in the right range so we don’t both waste our time—is that correct?”
Most likely you’ll get a yes. Then follow up with something like this:

“You must have a range that you’ve budgeted for this particular position, right?”
Again, you should get a yes. If you’re brave, just pause here and don’t say anything else. You may then get them to answer with the range, but if you aren’t brave or they aren’t volunteering any information, you can follow up with this:

“Well, if you tell me what the range is, even though I don’t know enough to state exactly what my salary requirements are, I can tell you whether or not the range matches up to what I’m looking for.”


## Conclusion

The book has some interesting topics, but it talks about a lot of things but don't get deep in any of the concepts. Some chapters seem to be there for no apparent reason other than talking about relevant experiences from the author (not a bad thing).
